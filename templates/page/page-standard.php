<?php while (have_posts()) : the_post(); ?>
<?php tha_entry_before(); ?>
<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php get_template_part('templates/structure/feature'); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">
		
		<?php tha_entry_top(); ?>   
		<div class="entry-content">
			<?php the_content(); ?>
		</div>
		<footer>
			<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>		

	</div><!-- /.main-inner -->	
</div><!-- /.main -->

<?php get_template_part('templates/structure/sidebar'); ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article>
<?php tha_entry_after(); ?>
<?php endwhile; ?>