<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo ws_title(); ?></span><br>
					<?php if ( function_exists('the_subtitle') ) {
						if ( get_post_meta($post->ID, 'endvr_feature_subtitle', true) ) { ?>
							<span class="feature-subtitle">
								<?php the_subtitle(); ?>
							</span><!-- /.feature-subtitle -->
						<?php }
					} ?>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<?php if ( function_exists('ws_breadcrumbs') ) { ?>
				<div class="feature-link span4 visible-desktop">
					<?php ws_breadcrumbs(); ?>
				</div><!-- /.feature-link -->
			<?php } ?>
		<?php tha_feature_bottom(); ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">

<?php tha_entry_before(); ?>

		<?php tha_entry_top(); ?>
		<div class="entry-content">
			<div class="sunday-profile">
				<h2><i class="icon-bookmark"></i>&nbsp; Sunday Event Profile</h2>
				<hr>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php
				if ($post->post_content=='') :
					echo 'This Sunday Event information will be posted soon&#133;<br>';
				else :
					the_content();
				endif;
				?>
				<?php endwhile; endif; ?>
			</div><!-- /.missionary-profile -->
		</div>
		<footer>
			 <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ws'), 'after' => '</p></nav>')); ?>
		</footer>
		<?php tha_entry_bottom(); ?>

<?php tha_entry_after(); ?>

	</div><!-- /.main-inner -->
</div><!-- /.main -->

<?php rewind_posts(); ?>

<?php while (have_posts()) : the_post(); ?>

<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>

		<?php if ( get_field('_endvr_sunday_event_details') ) : ?>

			<section id="endvr-widget-sunday-events" class="endvr-widget-sunday-events widget">
				<div class="widget-inner">

					<h2 class="widget-title"><i class="icon- ss-icon ss-checkclipboard"></i>&nbsp;Sunday Events</h2>
					<hr>

					<?php while( has_sub_field('_endvr_sunday_event_details') ) : ?>

					<div class="well">

						<h3><?php echo get_sub_field('_endvr_sunday_event_title'); ?></h3>

						<h5 class="min-event-411">
							<?php if ( get_sub_field('_endvr_sunday_event_time') ) { ?>
								<div class="min-event-time">
									<i class="icon- ss-icon ss-clock">&nbsp;</i>
									<?php the_sub_field('_endvr_sunday_event_time'); ?>
								</div>
							<?php } ?>

							<?php if ( get_sub_field('_endvr_sunday_event_date') ) { ?>
								<div class="min-event-date">
									<i class="icon- ss-icon ss-calendar">&nbsp;</i>
									<?php the_sub_field('_endvr_sunday_event_date'); ?>
								</div>
							<?php } ?>

							<?php if ( get_sub_field('_endvr_sunday_event_location') ) { ?>
								<div class="min-event-location">
									<i class="icon- ss-icon ss-location">&nbsp;</i>
									<?php the_sub_field('_endvr_sunday_event_location'); ?>
								</div>
							<?php } ?>

							<?php if ( get_sub_field('_endvr_sunday_event_demographic') ) { ?>
								<div class="min-event-demographic">
									<i class="icon- ss-icon ss-usergroup">&nbsp;</i>
									<?php the_sub_field('_endvr_sunday_event_demographic'); ?>
								</div>
							<?php } ?>
						</h5>

						<?php if ( get_sub_field('_endvr_sunday_event_description') ) { ?>
							<div class="min-event-description">
								<?php the_sub_field('_endvr_sunday_event_description'); ?>
							</div>
						<?php } ?>

					</div><!-- .well -->

					<?php endwhile; else : ?>

				</div><!-- /.widget-inner -->
			</section><!-- /.endvr-widget-sunday-events -->

		<?php endif; ?>

	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>

<?php endwhile; ?>

<?php get_template_part('templates/structure/content-bottom-after'); ?>

</article><!-- /article -->
<?php tha_entry_after(); ?>