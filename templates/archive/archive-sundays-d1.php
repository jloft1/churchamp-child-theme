<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title">Sunday Event Schedule</span><br>
					<span class="feature-subtitle">Join us for Worship and Sunday School</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->		
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span8'); ?> role="main">
	<div class="main-inner">
	
		<?php 
		$sun_args = array ( 'pagename' => 'sunday-schedule' );		
		$sun_page = new WP_Query( $sun_args );
		while ( $sun_page->have_posts() ) : $sun_page->the_post();
		the_content();
		endwhile;
		wp_reset_postdata();
		?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->
		
<?php tha_sidebars_before(); ?>
<aside id="sidebar" <?php ws_sidebar_class('span4'); ?> role="complementary">
	<div class="sidebar-inner">
		<?php tha_sidebar_top(); ?>
		
		<?php
		$ws_grid_columns_sd = 1;
		$ws_span_size_sd = ws_grid_class( $ws_grid_columns_sd );
		$posts_per_page = -1;
		$query_string = array(
		'post_type' => 'sundays',
		'posts_per_page' => -1,
		'orderby' => 'menu_order', 
		'order' => 'ASC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-sundays' );
				if( $ws_item_counter % $ws_grid_columns_sd == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns_sd == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no Sunday Events to display.</p>';
		}
		?>	
	
	     <?php tha_sidebar_bottom(); ?>
	</div><!-- /.sidebar-inner -->
</aside><!-- /.sidebar -->
<?php tha_sidebars_after(); ?>		

<?php get_template_part('templates/structure/content-bottom-after'); ?>