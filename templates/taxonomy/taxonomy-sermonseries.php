<?php 
$series = $wp_query->queried_object;
$series_link = '<a href="/sermons/' . $series->slug . '" title="' . sprintf(__('View sermon archive for this sermon series: %s', 'my_localization_domain'), $series->name) . '">' . $series->name . '</a>';
?>
		
<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo $series_link; ?></span><br>
					<span class="feature-subtitle">Sermon Series Archive</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-menu span4 visible-desktop">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br>
				<?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->			
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">
	
		<div class="row hidden-desktop">
			
			<div class="span12 feature-menu">
				<span class="feature-subtitle">Find a Sermon by&hellip;</span><br class="visible-phone"> <?php get_template_part('templates/menus/menu-sermons'); ?>
			</div><!-- /.feature-menu -->
		
		</div><!-- /.row -->
		
		<hr class="hidden-desktop">	
		
		<h2>Sermons in the Series: <em><?php echo $series->name; ?></em></h2>
		
		<hr>
	
		<?php
		$ss = $series->slug;
		$ws_grid_columns = 3;
		$ws_span_size = ws_grid_class( $ws_grid_columns );
		$posts_per_page = -1;
		$query_string = array(
		'sermonseries' => $ss,
		'posts_per_page' => -1,
		'orderby' => 'date', 
		'order' => 'DESC'
		);
		query_posts( $query_string );
		$ws_item_counter = 1;
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				if( $ws_item_counter == 1 ) ws_open_row();
				get_template_part( 'templates/grids/grid-sermons' );
				if( $ws_item_counter % $ws_grid_columns == 0 ) ws_close_row();
				if( $ws_item_counter % $ws_grid_columns == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
				$ws_item_counter++;
			}
			if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
		} else {
			echo '<p>Apologies, but there are no sermons to display.</p>';
		}
		?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>