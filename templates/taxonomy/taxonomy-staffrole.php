<?php
$role = $wp_query->queried_object;
$role_link = '<a href="/staff/' . $role->slug . '" title="' . sprintf(__('View the Profile for this Staff Member: %s', 'my_localization_domain'), $role->name) . '">' . $role->name . '</a>';
?>
		
<?php tha_feature_before(); ?>
<header <?php ws_feature_class(); ?>>
	<div class="container">
		<div class="row">
		<?php tha_feature_top(); ?>
			<div class="feature-header span8">
				<h1>
					<span class="feature-title"><?php echo $role->name; ?></span><br>
					<span class="feature-subtitle">Staff Directory by Role</span>
				</h1>
			</div><!-- /.feature-header -->
			<div class="feature-search span4 visible-desktop">
				<?php get_template_part('templates/meta/searchform'); ?>
			</div><!-- /.feature-search -->
			<div class="feature-link span4 visible-desktop">
				Click on photo for full profile
			</div><!-- /.feature-link --> 			
		<?php tha_feature_bottom(); ?>	
		</div><!-- /.row -->
	</div><!-- /.container -->		
</header><!-- /.feature -->
<?php tha_feature_after(); ?>

<?php get_template_part('templates/structure/content-before-top'); ?>

<div id="main" <?php ws_main_class('span12'); ?> role="main">
	<div class="main-inner">	
			
			<?php
			$role_g = $role->slug;
			$ws_grid_columns_staff = 4;
			$ws_span_size_staff = ws_grid_class( $ws_grid_columns_staff );
			$ss_g_query_string = array(
			'staffrole' => $role_g,
			'posts_per_archive_page' => 100, // offset will not work unless this is a postive integer
			'orderby' => 'menu_order', 
			'order' => 'ASC'
			);
			$ss_grid = new WP_Query( $ss_g_query_string );
			$ws_item_counter = 1;
			if ($ss_grid->have_posts()) {
				while ($ss_grid->have_posts()) {
					$ss_grid->the_post(); $do_not_duplicate = $post->ID;
					if( $ws_item_counter == 1 ) ws_open_row();
					get_template_part( 'templates/grids/grid-staff' );
					if( $ws_item_counter % $ws_grid_columns_staff == 0 ) ws_close_row();
					if( $ws_item_counter % $ws_grid_columns_staff == 0 && $posts_per_page != $ws_item_counter ) ws_open_row();
					$ws_item_counter++;
				}
				if( ($ws_item_counter-1) != $posts_per_page ) ws_close_row();
			} else {
				echo '<p>There are no Staff Members in the '.$role->name.' role.</p>';
			}
			?>

	</div><!-- /.main-inner -->		
</div><!-- /.main -->

<?php get_template_part('templates/structure/content-bottom-after'); ?>