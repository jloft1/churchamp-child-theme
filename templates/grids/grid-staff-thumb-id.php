<?php
/**
 * The template used for displaying staff in a grid.
 */
$ws_grid_columns_staff = 4;
$ws_span_size_staff = ws_grid_class( $ws_grid_columns_staff );
$ws_thumb_id = get_field('_endvr_staff_photo_thumb');
$ws_thumb_size = 'medium';
$ws_thumb = wp_get_attachment_image_src( $ws_thumb_id, $ws_thumb_size );
?>
<div class="grid-item <?php echo $ws_span_size_staff; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<img class="gi-img" src="<?php echo $ws_thumb[0]; ?>" alt="<?php the_title(); ?> <?php the_field('_endvr_staff_role'); ?>">
			<header class="gi-heading">
				<h3 class="gi-title"><?php the_title(); ?></h3>
				<span class="gi-tagline"><?php the_field('_endvr_staff_role'); ?></span>
			</header>
		</article><!-- #post-<?php the_ID(); ?> -->
	</a><!-- end anchor -->
</div><!-- .grid-item (end) -->