<?php
/**
 * The template used for displaying sunday event taxonomy in a grid.
 */
$ws_grid_columns = 4;
$ws_span_size = ws_grid_class( $ws_grid_columns ); 
?>

<?php if ( get_field('_endvr_sunday_event_details') ) : ?>
<?php while( has_sub_field('_endvr_sunday_event_details') ) : ?>

<div class="grid-item <?php echo $ws_span_size; ?>">
	<a class="gi-anchor well" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>					
			<!--<img class="gi-img" src="" alt="<?php the_title(); ?>">-->
			<header class="gi-heading">
				<h3 class="gi-title"><?php echo get_sub_field('_endvr_sunday_event_title'); ?></h3>
			</header>	
			<div class="gi-details">						
				<?php if ( get_sub_field('_endvr_sunday_event_time') ) { ?>
					<div class="min-event-time">
						<i class="icon- ss-icon ss-clock">&nbsp;</i>
						<?php the_sub_field('_endvr_sunday_event_time'); ?>
					</div>
				<?php } ?>		
				
				<?php if ( get_sub_field('_endvr_sunday_event_date') ) { ?>	
					<div class="min-event-date">
						<i class="icon- ss-icon ss-calendar">&nbsp;</i>
						<?php the_sub_field('_endvr_sunday_event_date'); ?>
					</div>
				<?php } ?>

				<?php if ( get_sub_field('_endvr_sunday_event_location') ) { ?>
					<div class="min-event-location">
						<i class="icon- ss-icon ss-location">&nbsp;</i>
						<?php the_sub_field('_endvr_sunday_event_location'); ?>
					</div>
				<?php } ?>
				
				<?php if ( get_sub_field('_endvr_sunday_event_demographic') ) { ?>
					<div class="min-event-demographic">
						<i class="icon- ss-icon ss-usergroup">&nbsp;</i>
						<?php the_sub_field('_endvr_sunday_event_demographic'); ?>
					</div>
				<?php } ?>								
			</div><!-- /.gi-details -->
		</article><!-- /#post-<?php the_ID(); ?> -->
	</a><!-- /.gi-anchor -->
</div><!-- /.grid-item -->

<?php endwhile; else : ?>
<?php endif; ?>