<?php

function symbolset_script() {
	wp_enqueue_script( 'ss_social_script', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social.js', array('jquery'), '', true );
	wp_enqueue_style( 'ss_social_style', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-social.css', array(), '1.0', 'screen,projection');
	wp_enqueue_script( 'ss_pika_script', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-pika.js', array('jquery'), '', true );
	wp_enqueue_style( 'ss_pika_style', get_stylesheet_directory_uri() . '/assets/fonts/symbolset/ss-pika.css', array(), '1.0', 'screen,projection');	
}
add_action('wp_enqueue_scripts', 'symbolset_script');